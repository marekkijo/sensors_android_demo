attribute vec3 aVertexPosition;

uniform mat4 mtxModelView;
uniform mat4 mtxProj;

void main(void) 
{
    gl_Position = mtxProj * mtxModelView * vec4( aVertexPosition, 1.0 );
}