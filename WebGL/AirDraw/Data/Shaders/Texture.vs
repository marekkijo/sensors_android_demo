attribute vec3 aVertexPosition;
attribute vec2 aTexCoord0;

uniform mat4 mtxModelView;
uniform mat4 mtxProj;

varying vec2 vTexCoord;

void main(void) 
{
	vTexCoord = aTexCoord0;
    gl_Position = mtxProj * mtxModelView * vec4( aVertexPosition, 1.0 );
}