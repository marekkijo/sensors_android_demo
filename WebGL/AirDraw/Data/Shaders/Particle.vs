attribute vec3 aVertexPosition;
attribute vec2 aTexCoord0;
attribute vec4 aColor;

uniform mat4 mtxModelView;
uniform mat4 mtxProj;

varying vec2 vTexCoord;
varying vec4 vColor;

void main(void) 
{
	vTexCoord = aTexCoord0;
	vColor = aColor;
    gl_Position = mtxProj * mtxModelView * vec4( aVertexPosition, 1.0 );
}