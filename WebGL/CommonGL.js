// CommonGL
var CCommonGL = function(canvasId)
{
	var self = this;
	self.canvasId = canvasId;
	
	self._private = 
	{
		canvas : document.getElementById( canvasId )
		,gl : null
		,EngineTimer : new CTimer()
		,FrameTimer : new CTimer()
		,fElapsedTime : 0.0
		,fFrameTime : 0.0
		,fAvgFrameTime : 0.0
		,Viewport : null
		,mtxProjection : mat4.create()
		,aMeshes : []
		,Camera : new CCamera()
	};

	self.Events = 
	{
		OnDraw : function(fFrameTime) {}
	};

	this.Init = function()
	{
		var gl = self._private.canvas.getContext("experimental-webgl");
        if (!gl) 
        {
            alert("Failed to initialise WebGL");
            return null;
        }

        self._private.gl = gl;

        self._private.Viewport = new CViewport( gl, self._private.canvas.width, self._private.canvas.height );
        self._private.Viewport.SetPerspective( 45, 0.1, 1000 );
        self._private.Viewport.Update();
        gl.enable( gl.DEPTH_TEST );

        self._private.EngineTimer.Start();

        return gl;
	}

	this.Update = function()
	{
		self._private.Camera.Update();
	}

	this.LoadShaderProgram = function(strVSUrl, strPSUrl)
	{
		var Loader = new CDataLoader();

		var strVS = Loader.Load( strVSUrl );
		var strPS = Loader.Load( strPSUrl );

		if( strVS == null || strPS == null )
		{
			alert( "Failed to load shader" );
			return null;
		}

		return self.CreateShaderProgramFromSource( strVS, strPS );
	}

	this.CreateShaderProgram = function(strVSId, strPSId)
	{
		var vs = self.CreateShaderFromElement( strVSId );
		var ps = self.CreateShaderFromElement( strPSId );

		if( vs == null || ps == null )
			return null;

		var gl = self._private.gl;
		var program = gl.createProgram();
		gl.attachShader( program, vs );
		gl.attachShader( program, ps );
		gl.linkProgram( program );

		if( !gl.getProgramParameter( program, gl.LINK_STATUS ) ) 
		{
            alert( "Shader program link error" );
            return null;
        }

        program.attribs = {};
        program.attribs.vertexPosition = gl.getAttribLocation( program, "aVertexPosition" );
        program.attribs.texCoord = gl.getAttribLocation( program, "aTexCoord0" );
        program.attribs.normal = gl.getAttribLocation( program, "aNormal" );
        program.attribs.binormal = gl.getAttribLocation( program, "aBinormal" );
        program.attribs.tangent = gl.getAttribLocation( program, "aTangent" );

        program.uniforms = {};
        program.uniforms.mtxWorld = gl.getUniformLocation( program, "mtxWorld" );
        program.uniforms.mtxProjection = gl.getUniformLocation( program, "mtxProjection" );
        program.uniforms.mtxProj = gl.getUniformLocation( program, "mtxProj" );
        program.uniforms.mtxView = gl.getUniformLocation( program, "mtxView" );
        program.uniforms.mtxMVP = gl.getUniformLocation( program, "mtxMVP" );
        program.uniforms.mtxModelView = gl.getUniformLocation( program, "mtxModelView" );

		console.log( "shader program created successfully" );
		return program;
	}

	this.CreateShaderProgramFromSource = function(strVS, strPS)
	{
		var vs = self.CreateShaderFromSource( strVS, "vertex" );
		var ps = self.CreateShaderFromSource( strPS, "pixel" );
		
		var gl = self._private.gl;
		var program = gl.createProgram();
		gl.attachShader( program, vs );
		gl.attachShader( program, ps );
		gl.linkProgram( program );

		if( !gl.getProgramParameter( program, gl.LINK_STATUS ) ) 
		{
            alert( "Shader program link error" );
            return null;
        }

        program.vs = vs;
        program.ps = ps;
        program.attribs = {};
        program.attribs.vertexPosition = gl.getAttribLocation( program, "aVertexPosition" );
        program.attribs.texCoord0 = gl.getAttribLocation( program, "aTexCoord0" );
        program.attribs.normal = gl.getAttribLocation( program, "aNormal" );
        program.attribs.binormal = gl.getAttribLocation( program, "aBinormal" );
        program.attribs.tangent = gl.getAttribLocation( program, "aTangent" );
        program.attribs.color = gl.getAttribLocation( program, "aColor" );

        program.uniforms = {};
        program.uniforms.mtxWorld = gl.getUniformLocation( program, "mtxWorld" );
        program.uniforms.mtxProjection = gl.getUniformLocation( program, "mtxProjection" );
        program.uniforms.mtxProj = gl.getUniformLocation( program, "mtxProj" );
        program.uniforms.mtxView = gl.getUniformLocation( program, "mtxView" );
        program.uniforms.mtxMVP = gl.getUniformLocation( program, "mtxMVP" );
        program.uniforms.mtxModelView = gl.getUniformLocation( program, "mtxModelView" );
        program.uniforms.texDiffuse = gl.getUniformLocation( program, "uTexDiffuse" );

        console.log( "shader program created successfully" );
		return program;
	}

	this.CreateShaderFromElement = function(strShaderId)
	{
		var ShaderEl = document.getElementById( strShaderId );
		if( ShaderEl == null )
		{
			alert( "Shader: " + strShaderId + " is null" );
			return null;
		}

		var str = "";
        var k = ShaderEl.firstChild;
        while (k) 
        {
            if (k.nodeType == 3) 
            {
                str += k.textContent;
            }
            k = k.nextSibling;
        }
        
        console.log( "creating shader from element: " + strShaderId );
        return self.CreateShaderFromSource( str, ShaderEl.type );
	}

	this.CreateShaderFromSource = function(strSource, strType)
	{
		var gl = self._private.gl;

		var shader;
        if( strType == "x-shader/x-fragment" || strType == "x-shader/x-pixel" || strType == "pixel" ) 
        {
            shader = self._private.gl.createShader( gl.FRAGMENT_SHADER );
        } 
        else if( strType == "x-shader/x-vertex" || strType == "vertex" ) 
        {
            shader = self._private.gl.createShader( gl.VERTEX_SHADER );
        } 
        else
        {
        	alsert( "Invalid shader type: " + strType );
        	return null;
        }

        gl.shaderSource( shader, strSource );
        gl.compileShader( shader );

        if ( !gl.getShaderParameter( shader, gl.COMPILE_STATUS ) ) 
        {
            alert( "Shader compile error:\n" + gl.getShaderInfoLog( shader ) );
            return null;
        }

        console.log( "shader: " + strType + " compiled successfully" );
        return shader;
	}

	this.CreateMaterial = function()
	{
		var Mat = new CMaterial( self );
		return Mat;
	}

	this.GetCamera = function()
	{
		return self._private.Camera;
	}

	this.CreateMesh = function()
	{
		return new CMesh( self );
	}

	this.GetViewport = function()
	{
		return self._private.Viewport;
	}

	this.GetFrameTime = function()
	{
		return self._private.fFrameTime;
	}

	this.GetAvgFrameTime = function()
	{
		return self._private.fAvgFrameTime;
	}

	this.GetEngineTime = function()
	{
		return self._private.fEngineTime;
	}

	this.GetGL = function()
	{
		//console.log( self );
		//console.log( self._private );
		return self._private.gl;
	}

	this.CreateTexture = function()
	{
		var Tex = new CTexture( self );
		return Tex;
	}

	var iFrameCount = 0;
	var fAccFrameTime = 0;

	this.RenderFrame = function()
	{
		self._private.FrameTimer.Start();
		self._private.Viewport.Update();
		self.Events.OnDraw( self._private.fFrameTime );
		self._private.fFrameTime = self._private.FrameTimer.GetElapsedTime();
		self._private.fEngineTime = self._private.EngineTimer.GetElapsedTime();

		fAccFrameTime += self._private.fFrameTime;
		if( ++iFrameCount > 10 )
		{
			self._private.fAvgFrameTime = ( fAccFrameTime * 0.1 ).toFixed( 2 );
			iFrameCount = 0;
			fAccFrameTime = 0;
		}
	}

	this.StartRendering = function(iInterval)
	{
		setInterval( self.RenderFrame, iInterval || 1 );
	}

	this.CreateParticleBuffer = function()
	{
		return new CParticleBuffer( self );
	}
};

var CCamera = function(gl)
{
	var self = this;
	self.gl = gl;
	self.mtxView = mat4.create();
	mat4.identity( self.mtxView );

	this.Update = function()
	{

	}
}

var CViewport = function(gl, iWidth, iHeight, afColor)
{
	var self = this;

	self._private = 
	{
		gl : null
		,iWidth : 0
		,iHeight : 0
		,afColor : [ 0.0, 0.0, 0.0, 1.0 ]
		,mtxProjection : mat4.create()
		,fFOV : 0
		,fNear : 0
		,fFar : 0
	};

	self._private.gl = gl;
	self._private.iWidth = iWidth || 640;
	self._private.iHeight = iHeight || 480;
	self._private.afColor = afColor || [ 0.5, 0.5, 0.5, 1.0 ];
	mat4.identity( self._private.mtxProjection );

	this.Update = function()
	{
		gl.viewport( 0, 0, self._private.iWidth, self._private.iHeight );
		gl.clearColor( self._private.afColor[ 0 ], self._private.afColor[ 1 ], self._private.afColor[ 2 ], self._private.afColor[ 3 ] );
		gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );
	}

	this.SetColor = function(fR, fG, fB, fA)
	{
		self._private.afColor = [ fR, fG, fB, fA ];
	}

	this.SetPerspective = function(fFOV, fNear, fFar)
	{
		self._private.fFOV = fFOV;
		self._private.fNear = fNear;
		self._private.fFar = fFar;
		self._private.fAspectRatio = self._private.iWidth / self._private.iHeight;
		mat4.perspective( fFOV, self._private.fAspectRatio, fNear, fFar, self._private.mtxProjection );
		return self._private.mtxProjection;
	}

	this.GetProjectionMatrix = function()
	{
		return self._private.mtxProjection;
	}
}

var CMesh = function(Engine)
{
	var self = this;

	self.mtxTransform = mat4.create();
	mat4.identity( self.mtxTransform );
	self.vecPosition = [ 0, 0, 0 ];
	self.VB = null;
	self.gl = Engine.GetGL();
	self.Engine = Engine;
	self.Material = null;

	this.CreateVertexBuffer = function()
	{
		self.VB = new CVertexBuffer( self.gl );

		return self.VB;
	}

	this.SetPosition = function(aPos)
	{
		self.vecPosition = aPos;
	}

	this.SetMaterial = function(Material)
	{
		self.Material = Material;
	}

	this.Draw = function()
	{
		var gl = self.gl;

		if( self.Material == null )
		{
			alert( "Material not set to the mesh" );
			return;
		}

		self.Material.Bind();

		mat4.identity( self.mtxTransform );
		mat4.translate( self.mtxTransform, self.vecPosition );
		var mtxProj = self.Engine.GetViewport().GetProjectionMatrix();

		gl.bindBuffer( gl.ARRAY_BUFFER, self.VB.VBO );

		gl.enableVertexAttribArray( self.Material.ShaderProgram.attribs.vertexPosition );
        gl.vertexAttribPointer( self.Material.ShaderProgram.attribs.vertexPosition, 3, gl.FLOAT, false, self.VB.GetVertexSize(), 0 );

        if( self.Material.ShaderProgram.attribs.texCoord0 > 0 )
        {
        	gl.enableVertexAttribArray( self.Material.ShaderProgram.attribs.texCoord0 );
            gl.vertexAttribPointer( self.Material.ShaderProgram.attribs.texCoord0, 2, gl.FLOAT, false, self.VB.GetVertexSize(), self.VB.GetOffset( "texcoord0" ) );
        }

        if( self.Material.ShaderProgram.attribs.color > 0 )
        {
        	gl.enableVertexAttribArray( self.Material.ShaderProgram.attribs.color );
        	gl.vertexAttribPointer( self.Material.ShaderProgram.attribs.color, 4, gl.FLOAT, false, self.VB.GetVertexSize(), self.VB.GetOffset( "color" ) );
        }

      	gl.uniformMatrix4fv( self.Material.ShaderProgram.uniforms.mtxModelView, false, self.mtxTransform );
      	gl.uniformMatrix4fv( self.Material.ShaderProgram.uniforms.mtxProj, false, mtxProj );

        gl.drawArrays( self.VB.TriangleMode, 0, self.VB.iNumItems );
	}

	this.Update = function()
	{
		var gl = self.gl;
		gl.bindBuffer( gl.ARRAY_BUFFER, self.VB.VBO );
		gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( self.VB.aVertices ), self.VB.DrawMode );
	}
}

var CVertexBuffer = function(gl)
{
	var self = this;
	self.gl = gl;
	self.iItemSize = 0;
	self.iNumItems = 0;
	self.aVertices = [];
	self.iVertexSize = 0; // size in bytes
	self.offsets = 
	{
		position : 0
	}; // vertex attrib offsets in bytes
	self.VBO = gl.createBuffer();

	self.TriangleMode = gl.TRIANGLES;
	self.DrawMode = gl.STATIC_DRAW;

	this.SetData = function(Args)
	{
		var gl = self.gl;
		self.aVertices = Args.aVertices;
		self.TriangleMode = Args.TriangleMode || gl.TRIANGLES;
		self.DrawMode = Args.DrawMode || gl.STATIC_DRAW;

		self.ParseAttribs( Args.VertexAttribs );

		self.gl.bindBuffer( self.gl.ARRAY_BUFFER, self.VBO );
		self.gl.bufferData( self.gl.ARRAY_BUFFER, new Float32Array( self.aVertices ), self.DrawMode );
	}

	this.ParseAttribs = function(VertexAttribs)
	{
		self.iItemSize = 3; // vertex-pos
		self.iVertexSize = 3 * 4;
		var iCurrOffset = 12;

		if( VertexAttribs )
		{
			if( VertexAttribs.bNormal )
			{
				self.iItemSize += 3;
				self.iVertexSize += 3 * 4;
				self.bIsNormal = true;
				self.offsets.normal = iCurrOffset;
				iCurrOffset += 12;
			}

			if( VertexAttribs.bBinormal )
			{
				self.iItemSize += 3;
				self.iVertexSize += 3 * 4;
				self.bIsBinormal = true;
				self.offsets.binormal = iCurrOffset;
				iCurrOffset += 12;
			}

			if( VertexAttribs.bTangent )
			{
				self.iItemSize += 3;
				self.iVertexSize += 3 * 4;
				self.bIsTangent = true;
				self.offsets.tangent = iCurrOffset;
				iCurrOffset += 12;
			}

			for(var i = 0; i < 8; ++i)
			{
				var tc = "bTexCoord" + i;
				if( VertexAttribs[ tc ] )
				{
					var is = "bIsTexCoord" + i;
					self[ is ] = true;
					self.iItemSize += 2;
					self.iVertexSize += 2 * 4;
					self.offsets[ "texcoord" + i ] = iCurrOffset;
					iCurrOffset += 8;
				}
			}

			if( VertexAttribs.bColor )
			{
				self.iItemSize += 4;
				self.iVertexSize += 4 * 4;
				self.bIsColor = true;
				self.offsets.color = iCurrOffset;
				iCurrOffset += 16;
			}
		}

		self.iNumItems = self.aVertices.length / self.iItemSize;
	}

	this.GetOffset = function(strName)
	{
		return self.offsets[ strName ];
	}

	this.GetVertexSize = function()
	{
		return self.iVertexSize;
	}

	this.GetVertexCount = function()
	{
		return self.iNumItems;
	}

	this.GetItemCount = function()
	{
		return self.iItemSize;
	}
}

var CTexture = function(Engine)
{
	var self = this;
	self.Engine = Engine;
	self.gl = Engine.GetGL();
	self.Img = new Image();
	self.glTex = self.gl.createTexture();
	self.glTexMode = self.gl.TEXTURE_2D;

	this.Load = function(strUrl, OnLoad)
	{
		console.log( "loading texture: " + strUrl );
		self.Img.onload = function()
		{
			var gl = self.gl;
			gl.bindTexture( self.glTexMode, self.glTex );
			gl.texImage2D( self.glTexMode, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, self.Img );
			gl.texParameteri( self.glTexMode, gl.TEXTURE_MAG_FILTER, gl.LINEAR );
			gl.texParameteri( self.glTexMode, gl.TEXTURE_MIN_FILTER, gl.LINEAR );
			gl.bindTexture( self.glTexMode, null );

			if( OnLoad ) OnLoad( self );

			console.log( "texture: " + strUrl + " loaded successfully" );
		}

		self.Img.src = strUrl;
		return self.glTex;
	}

	this.Bind = function()
	{
		self.gl.bindTexture( gl.TEXTURE_2D, self.glTex );
	}

	this.Unbind = function()
	{
		self.gl.bindTexture( gl.TEXTURE_2D, null );
	}
}

var CMaterial = function(Engine)
{
	var self = this;
	self.Engine = Engine;
	self.gl = Engine.GetGL();
	self.Texture = null;
	self.ShaderProgram = null;

	this.SetShaderProgram = function(ShaderProgram)
	{
		self.ShaderProgram = ShaderProgram;
	}

	this.SetTexture = function(Tex)
	{
		self.Texture = Tex;
	}

	this.Bind = function()
	{
		var gl = self.gl;
		gl.useProgram( self.ShaderProgram );
		if( self.Texture )
		{
			gl.activeTexture( gl.TEXTURE0 );
			gl.bindTexture( self.Texture.glTexMode, self.Texture.glTex );
			gl.uniform1i( self.ShaderProgram.uniforms.uTexDiffuse, 0 );
		}
	}

	this.Unbind = function()
	{
		var gl = self.gl;
		gl.useProgram( null );
		if( self.Texture )
			gl.bindTexture( self.Texture.glTexMode, null );
	}
}

var CTimer = function()
{
	var self = this;
	self.m_fStartTimestamp = 0;
	self.m_fEndTimestamp = 0;

	this.Start = function()
	{
		self.m_fStartTimestamp = Date.now(); 
	}

	this.Stop = function()
	{
		self.m_fEndTimestamp = Date.now();
		return self.m_fEndTimestamp - self.m_fStartTimestamp;
	}

	this.GetElapsedTime = function()
	{
		var tmp1 = Date.now();
		return tmp1 - self.m_fStartTimestamp;
	}
}

var CDataLoader = function()
{
	var self = this;
	self.HttpRequest = new XMLHttpRequest();

	this.Load = function(strUrl)
	{
		self.HttpRequest.open( "GET", strUrl, false );
		self.HttpRequest.send();
		self.bLoaded = true;
		return self.HttpRequest.responseText;
	}

	this.LoadAsync = function(strUrl, OnLoad, OnError)
	{
		self.HttpRequest.onreadystatechange = function() { if( self.IsRready() ) OnLoad(); };
		self.HttpRequest.timeout = 3000;
		self.HttpRequest.open( "GET", strUrl, true );
		self.HttpRequest.send();
	}

	this.GetData = function()
	{
		return self.HttpRequest.responseText;
	}

	this.IsRready = function()
	{
		return self.HttpRequest.readyState == 4;
	}

	this.GetStatus = function()
	{
		return self.HttpRequest.status;
	}
}