// Particle class
var CParticle = function(CParticleBuffer, iId)
{
	var self = this;
	self.vecPosition = [ 0, 0, 0 ];
	self.fScale = 1.0;
	self.vecColor = [ 1, 1, 1, 1 ];
	self.iId = iId;
	self.Buffer = CParticleBuffer;
};

var CParticleBuffer = function(Engine)
{
	var self = this;
	self.Engine = Engine;
	self.gl = Engine.GetGL();
	self.aParticles = [];
	self.Mesh = null;

	self.Create = function(Material, iSize, aPositions)
	{
		var Particle;
		self.aParticles = [];

		// Vertex buffer
		self.QuadVB = [
         1.0,  1.0,  0.0,   1, 1,	1, 1, 1, 1,
		-1.0,  1.0,  0.0,   0, 1,	1, 1, 1, 1,
		 1.0, -1.0,  0.0,   1, 0,	1, 1, 1, 1,

		 1.0, -1.0,  0.0,   1, 0,	1, 1, 1, 1,
		-1.0,  1.0,  0.0,   0, 1,	1, 1, 1, 1,
		-1.0, -1.0,  0.0,   0, 0,	1, 1, 1, 1 
		];

		var VB = [];

		for(var i = 0; i < iSize; ++i)
		{
			Particle = new CParticle( self, self.aParticles.length );
			if( aPositions )
				Particle.vecPosition = aPositions[ i ];
			self.aParticles.push( Particle );

			VB = VB.concat( self.QuadVB );
		}

		self.Mesh = self.Engine.CreateMesh();
		self.Mesh.SetMaterial( Material );
		self.Mesh.SetPosition( [ 0, 0, -10 ] );
		var VBuff = self.Mesh.CreateVertexBuffer();
		VBuff.SetData({
			aVertices : VB
			,DrawMode : self.gl.STATIC_DRAW
			,VertexAttribs : { bTexCoord0 : true, bColor : true }
		});
	}

	this.GetMesh = function()
	{
		return self.Mesh;
	}

	this.Update = function()
	{
		var iCurrVertex = 0;

		for(var i = 0; i < self.aParticles.length; ++i)
		{
			var Part = self.aParticles[ i ];
			//console.log( Part );
			// Position offset
			var iVertexOffset = i * self.Mesh.VB.iVertexSize;

			for(var v = 0; v < 6; ++v)
			{
				// Position
				self.Mesh.VB.aVertices[ iCurrVertex + 0 ] = self.QuadVB[ v * 9 + 0 ] + Part.vecPosition[ 0 ];
				self.Mesh.VB.aVertices[ iCurrVertex + 1 ] = self.QuadVB[ v * 9 + 1 ] + Part.vecPosition[ 1 ];
				self.Mesh.VB.aVertices[ iCurrVertex + 2 ] = self.QuadVB[ v * 9 + 2 ] + Part.vecPosition[ 2 ];
				// TexCoord0
				self.Mesh.VB.aVertices[ iCurrVertex + 3 ];
				self.Mesh.VB.aVertices[ iCurrVertex + 4 ];
				// Color
				self.Mesh.VB.aVertices[ iCurrVertex + 5 ] = Part.vecColor[ 0 ];
				self.Mesh.VB.aVertices[ iCurrVertex + 6 ] = Part.vecColor[ 1 ];
				self.Mesh.VB.aVertices[ iCurrVertex + 7 ] = Part.vecColor[ 2 ];
				self.Mesh.VB.aVertices[ iCurrVertex + 8 ] = Part.vecColor[ 3 ];

				iCurrVertex += self.Mesh.VB.GetItemCount();
			}
		}

		self.Mesh.Update();
	}

	this.Draw = function()
	{
		gl.enable( gl.BLEND );
        gl.blendFunc( gl.SRC_ALPHA, gl.ONE );
        gl.disable( gl.DEPTH_TEST );

		self.Mesh.Draw();

		gl.disable( gl.BLEND );
		gl.enable( gl.DEPTH_TEST );
	}

	this.ForEachParticle = function(Handler)
	{
		for(var i = 0; i < self.aParticles.length; ++i)
		{
			Handler( self.aParticles, i );
		}
	}
};