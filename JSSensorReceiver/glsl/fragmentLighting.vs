attribute vec4 glt_Vertex;
attribute vec4 glt_Normal;

uniform mat4 glt_ModelView;
uniform mat4 glt_Projection;

varying vec3 glt_vPosition;
varying vec3 glt_vNormal;

void main()
{
    mat4 mvp = glt_Projection * glt_ModelView;
    gl_Position = mvp * glt_Vertex;
    glt_vPosition = (glt_ModelView * glt_Vertex).xyz;
    glt_vNormal = (glt_ModelView * glt_Normal).xyz;
}
