precision mediump float;

uniform samplerCube glt_Texture0;

varying vec3 glt_vMultiTexCoord0;

void main() {
	gl_FragColor = textureCube(glt_Texture0, glt_vMultiTexCoord0);
}
