attribute vec4 glt_Vertex;
attribute vec3 glt_Normal;

uniform mat4 glt_ModelView;
uniform mat4 glt_Projection;
uniform mat4 glt_NormalMatrix;

varying vec3 glt_vEye;
varying vec3 glt_vNormal;

void main() {
	mat4 mvp = glt_Projection * glt_ModelView;
	gl_Position = mvp * glt_Vertex;
	glt_vEye = (glt_ModelView * glt_Vertex).xyz;
	glt_vNormal = mat3(glt_NormalMatrix) * glt_Normal;
}
