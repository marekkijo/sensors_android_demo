precision mediump float;

uniform mat4 glt_ModelViewT;
uniform samplerCube glt_Texture0;

varying vec3 glt_vEye;
varying vec3 glt_vNormal;

vec4 lerp(vec4 a, vec4 b, float s) {
	return vec4(a + (b - a) * s);       
}

void main() {
	float fresnel = 1.0;
	vec3 eta = vec3(0.68, 0.66, 0.64);
	vec3 E = normalize(glt_vEye);
	vec3 N = normalize(glt_vNormal);
	vec3 reflectEN = reflect(E, N);
	vec3 refractENr = refract(E, N, eta.r);
	vec3 refractENg = refract(E, N, eta.g);
	vec3 refractENb = refract(E, N, eta.b);
	reflectEN = mat3(glt_ModelViewT) * reflectEN;
	refractENr = mat3(glt_ModelViewT) * refractENr;
	refractENg = mat3(glt_ModelViewT) * refractENg;
	refractENb = mat3(glt_ModelViewT) * refractENb;
	
	vec4 reflectColor = textureCube(glt_Texture0, reflectEN);
	vec4 refractColor = vec4(textureCube(glt_Texture0, refractENr).r,
	                         textureCube(glt_Texture0, refractENg).g,
	                         textureCube(glt_Texture0, refractENb).b,
	                         1.0);
	float fresnelFactor = clamp(abs(dot(E, N) * fresnel), 0.0, 1.0);
	gl_FragColor = lerp(reflectColor, refractColor, fresnelFactor);
}
