precision mediump float;

uniform samplerCube glt_Texture0;

varying vec3 glt_vPosition;
varying vec3 glt_vNormal;

void main()
{
    vec3 P = normalize(glt_vPosition);
    vec3 N = normalize(glt_vNormal);
    gl_FragColor.xyz = reflect(P, N);
    gl_FragColor.a = 1.0;
}
