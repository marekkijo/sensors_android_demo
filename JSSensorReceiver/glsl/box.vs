attribute vec4 glt_Vertex;
attribute vec4 glt_Normal;
attribute vec2 glt_MultiTexCoord0;

uniform mat4 glt_ModelView;
uniform mat4 glt_Projection;

varying vec2 glt_vMultiTexCoord0;

void main() {
	mat4 mvp = glt_Projection * glt_ModelView;
	gl_Position = mvp * glt_Vertex;
	glt_vMultiTexCoord0 = glt_MultiTexCoord0;
}
