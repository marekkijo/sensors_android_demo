package com.marekkijo.sensorbroadcaster;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements SensorEventListener {
    private static final float _lowPassThreshold = 0.5f;
    private SensorManager _sensorManager;

    private Sensor _accSensor;
    private Sensor _gyroSensor;
    private Sensor _magSensor;

    private float[] _accData;
    private float[] _magData;
    private float[] _gyrData;
    private float[] _resetData;

    private boolean _connected;

    private int _checker = 0;
    private float _RTmp[] = new float[9];
    private float _Rot[] = new float[9];
    private float _I[] = new float[9];
    private float _results[] = new float[3];

    private float _azimuth;
    private float _pitch;
    private float _roll;

    private long _lastTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _connected = false;

        setContentView(R.layout.activity_main);

        findViewById(R.id.server_button).setOnClickListener(serverButtonOnClickListener);
        findViewById(R.id.reset_button).setOnClickListener(resetButtonOnClickListener);

        for (float v : _accData = new float[3])
            v = 0.0f;
        for (float v : _magData = new float[3])
            v = 0.0f;
        for (float v : _gyrData = new float[3])
            v = 0.0f;
        for (float v : _resetData = new float[3])
            v = 0.0f;

        _sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        _accSensor = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (_accSensor != null)
            _sensorManager.registerListener(this, _accSensor, SensorManager.SENSOR_DELAY_NORMAL);
        _gyroSensor = _sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if (_gyroSensor != null)
            _sensorManager.registerListener(this, _gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
        _magSensor = _sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if (_magSensor != null)
            _sensorManager.registerListener(this, _magSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (_accSensor != null)
            _sensorManager.registerListener(this, _accSensor, SensorManager.SENSOR_DELAY_NORMAL);
        if (_gyroSensor != null)
            _sensorManager.registerListener(this, _gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
        if (_magSensor != null)
            _sensorManager.registerListener(this, _magSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        _sensorManager.unregisterListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WebSocketJNILib.destroy();
        _sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (!_connected)
            return;

        String messageStr = "";

        switch (sensorEvent.sensor.getType()){
            case Sensor.TYPE_ACCELEROMETER:
                _accData = lowPass(sensorEvent.values.clone(), _accData);
                _checker ++;
                messageStr = "acc " + _accData[0] + " " + _accData[1] + " " + _accData[2];
                WebSocketJNILib.sendString(messageStr);
                break;/*
            case Sensor.TYPE_MAGNETIC_FIELD:
                _magData = lowPass(sensorEvent.values.clone(), _magData);
                _checker ++;
                messageStr = "mag " + _magData[0] + " " + _magData[1] + " " + _magData[2];
                WebSocketJNILib.sendString(messageStr);
                break;*/
            case Sensor.TYPE_GYROSCOPE:
                _gyrData = lowPass(sensorEvent.values.clone(), _gyrData);
                _checker ++;
                messageStr = "gyr " + _gyrData[0] + " " + _gyrData[1] + " " + _gyrData[2];
                WebSocketJNILib.sendString(messageStr);
                break;
        }
        //if(_checker >= 2) {
            if (_accData != null && _gyrData != null) {
                complementaryFilter(_accData, _gyrData);
            }/*
            if (_accData != null && _magData != null) {
                SensorManager.getRotationMatrix(_RTmp, _I, _accData, _magData);

                SensorManager.remapCoordinateSystem(_RTmp, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_Z, _Rot);

                SensorManager.getOrientation(_Rot, _results);

                _azimuth = (float) (((_results[0] * 180) / Math.PI) + 180);
                _pitch = (float) (((_results[1] * 180 / Math.PI)) + 90);
                _roll = (float) (((_results[2] * 180 / Math.PI)));
            }*/
            messageStr = "rot " + ((_azimuth - _resetData[0]) * 4000.0) + " " + ((_pitch - _resetData[1]) * 4000.0) + " " + ((_roll - _resetData[2]) * 4000.0);
            WebSocketJNILib.sendString(messageStr);
            _checker = 0;
        //}
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    protected float[] lowPass(float[] input, float[] output) {
        if (output == null) return input;

        for (int i = 0; i < input.length; ++ i)
            output[i] = output[i] + _lowPassThreshold * (input[i] - output[i]);
        return output;
    }

    private static final float ACCELEROMETER_SENSITIVITY = 8192.0f;
    private static final float GYROSCOPE_SENSITIVITY = 65.536f;

    protected void complementaryFilter(float[] accData, float[] gyrData) {
        long thisTime = System.currentTimeMillis();
        if (_lastTime == 0) {
            _lastTime = thisTime;
            return;
        }

        float _dt = (thisTime - _lastTime) / 1000.0f;
        _lastTime = thisTime;
        float pitchAcc, rollAcc;

        // Integrate the gyroscope data -> int(angularSpeed) = angle
        _pitch += (gyrData[0] / GYROSCOPE_SENSITIVITY) * _dt; // Angle around the X-axis
        _roll -= (gyrData[1] / GYROSCOPE_SENSITIVITY) * _dt;    // Angle around the Y-axis

        // Compensate for drift with accelerometer data if !bullshit
        // Sensitivity = -2 to 2 G at 16Bit -> 2G = 32768 && 0.5G = 8192
        int forceMagnitudeApprox = (int)(Math.abs(accData[0]) + Math.abs(accData[1]) + Math.abs(accData[2]));
        if (forceMagnitudeApprox > 8192 && forceMagnitudeApprox < 32768)
        {
            // Turning around the X axis results in a vector on the Y-axis
            pitchAcc = (float)(Math.atan2(accData[1], accData[2]) * 180.0f / Math.PI);
            //_pitch = _pitch * 0.98f + pitchAcc * 0.02f;
            _pitch = _pitch * 0.5f + pitchAcc * 0.5f;

            // Turning around the Y axis results in a vector on the X-axis
            rollAcc = (float)(Math.atan2(accData[0], accData[2]) * 180.0f / Math.PI);
            //_roll = _roll * 0.98f + rollAcc * 0.02f;
            _roll = _roll * 0.5f + rollAcc * 0.5f;
        }
    }

    private View.OnClickListener serverButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Button serverButton = (Button) findViewById(R.id.server_button);
            if (_connected) {
                _connected = false;
                WebSocketJNILib.destroy();
                serverButton.setText(R.string.start_server);
            } else {
                if (WebSocketJNILib.init(1234) && WebSocketJNILib.connect()) {
                    _connected = true;
                    serverButton.setText(R.string.stop_server);
                }
            }
        }
    };

    private View.OnClickListener resetButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            _resetData[0] = _azimuth;
            _resetData[1] = _pitch;
            _resetData[2] = _roll;
        }
    };
}
