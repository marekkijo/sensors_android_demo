package com.marekkijo.sensorbroadcaster;

public class WebSocketJNILib {
    static {
        System.loadLibrary("websocket");
    }

    public static native boolean init(int port);
    public static native boolean connect();
    public static native void destroy();
    public static native String getString();
    public static native void sendString(String str);
}
