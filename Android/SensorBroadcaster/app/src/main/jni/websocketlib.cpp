#include <jni.h>
#include "websocketserver.h"

static websocket::WebSocketServer *wss = 0;

extern "C" {
    JNIEXPORT jboolean JNICALL Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_init(JNIEnv* env, jobject obj, jint port);
    JNIEXPORT jboolean JNICALL Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_connect(JNIEnv* env, jobject obj);
    JNIEXPORT void JNICALL Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_destroy(JNIEnv* env, jobject obj);
    JNIEXPORT jstring JNICALL Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_getString(JNIEnv* env, jobject obj);
    JNIEXPORT void JNICALL Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_sendString(JNIEnv* env, jobject obj, jstring str);
};

JNIEXPORT jboolean JNICALL
Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_init(JNIEnv* env, jobject obj, jint port) {
    if (wss) {
        delete wss;
        wss = 0;
    }

    wss = new websocket::WebSocketServer();
    return wss->init(port) ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jboolean JNICALL
Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_connect(JNIEnv* env, jobject obj) {
    if (!wss)
    	return JNI_FALSE;
    return wss->connect() ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT void JNICALL
Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_destroy(JNIEnv* env, jobject obj) {
    if (wss) {
        delete wss;
        wss = 0;
    }
}

JNIEXPORT jstring JNICALL
Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_getString(JNIEnv* env, jobject obj) {
    std::string stdstr;
    if (wss)
    	std::string stdstr = wss->getString();
    return env->NewStringUTF(stdstr.c_str());
}

JNIEXPORT void JNICALL
Java_com_marekkijo_sensorbroadcaster_WebSocketJNILib_sendString(JNIEnv* env, jobject obj, jstring str) {
    if (!wss || !str)
    	return;

    const char *s = env->GetStringUTFChars(str, 0);
    std::string stdstr = s;
    env->ReleaseStringUTFChars(str, s);

    wss->sendString(stdstr);
}
