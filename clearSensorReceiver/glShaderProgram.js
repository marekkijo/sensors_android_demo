function ShaderProgram() {
	if (!gl) {
		console.error("ShaderProgram cannot work withour \"gl\" (experimental-webgl instance) initialized.");
		return;
	}
	this.id = null;
	this.vsID = null;
	this.fsID = null;
	this.uniformNormal = null;
	this.uniformColor = null;
	this.uniformModelView = null;
	this.uniformProjection = null;
	this.uniformMVP = null;
	this.uniformNormalMatrix = null;
	this.uniformModelViewT = null;
	this.uniformLightPosition0 = null;
	this.uniformTexture0 = null;
	this.uniformTexture1 = null;
	this.uniformTexture2 = null;
	this.uniformTexture3 = null;
}

ShaderProgram.prototype.compileShaderResourcesAndLinkProgram = function(vrname, frname) {
	return this.compileShaderResources(vrname, frname) && this.linkProgram();
}

ShaderProgram.prototype.compileShaderSourceAndLinkProgram = function(vsource, fsource) {
	return this.compileVShaderSource(vsource) && this.compileFShaderSource(fsource) && this.linkProgram();
}

ShaderProgram.prototype.compileShaderResources = function(vrname, frname) {
	return this.compileVShaderResource(vrname) && this.compileFShaderResource(frname);
}

ShaderProgram.prototype.compileVShaderResource = function(rname) {
	var source = this.readShaderResource(rname);
	return source && this.compileVShaderSource(source);
}

ShaderProgram.prototype.compileFShaderResource = function(rname) {
	var source = this.readShaderResource(rname);
	return source && this.compileFShaderSource(source);
}

ShaderProgram.prototype.readShaderResource = function(rname) {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', rname, false);
	xhr.overrideMimeType('text/plain');
	xhr.send(null);
	if (xhr.readyState == xhr.DONE && xhr.status === 200)
		return xhr.responseText;
	console.error("Could not get resource: " + rname);
}

ShaderProgram.prototype.compileVShaderSource = function(source) {
	this.vsID = gl.createShader(gl.VERTEX_SHADER);
	if (!this.vsID) {
		console.error("Could not create vertex shader.");
		return false;
	}
	if (!this.compileShaderSource(this.vsID, source)) {
		gl.deleteShader(this.vsID);
		this.vsID = null;
		return false;
	}
	return true;
}

ShaderProgram.prototype.compileFShaderSource = function(source) {
	this.fsID = gl.createShader(gl.FRAGMENT_SHADER);
	if (!this.fsID) {
		console.error("Could not create fragment shader.");
		return false;
	}
	if (!this.compileShaderSource(this.fsID, source)) {
		gl.deleteShader(this.fsID);
		this.fsID = null;
		return false;
	}
	return true;
}

ShaderProgram.prototype.compileShaderSource = function(shaderID, source) {
	gl.shaderSource(shaderID, source);
	gl.compileShader(shaderID);
	if (!gl.getShaderParameter(shaderID, gl.COMPILE_STATUS)) {
		console.error("COMPILE_STATUS: " + gl.getShaderInfoLog(shaderID) + "SOURCE:\n" + source);
		return false;
	}
	return true;
}

ShaderProgram.prototype.linkProgram = function() {
	if (!this.vsID || !this.fsID) {
		console.error("One or both of shaders were not loaded.");
		return false;
	}
	this.id = gl.createProgram();
	if (!this.id) {
		console.error("Could not create shader program.");
		return false;
	}
	this.bindAttribs();
	gl.attachShader(this.id, this.vsID);
	gl.attachShader(this.id, this.fsID);
	gl.linkProgram(this.id);
	if (!gl.getProgramParameter(this.id, gl.LINK_STATUS)) {
		console.error("LINK_STATUS: " + gl.getProgramInfoLog(this.id));
		gl.deleteProgram(this.id);
		this.id = null;
		return false;
	}
	this.bindUniforms();
	return true;
}

ShaderProgram.prototype.bindAttribs = function() {
	gl.bindAttribLocation(this.id, ShaderProgram.ATTRIB_VERTEX, ShaderProgram.ATTRIB_VERTEX_NAME);
	gl.bindAttribLocation(this.id, ShaderProgram.ATTRIB_NORMAL, ShaderProgram.ATTRIB_NORMAL_NAME);
	gl.bindAttribLocation(this.id, ShaderProgram.ATTRIB_COLOR, ShaderProgram.ATTRIB_COLOR_NAME);
	gl.bindAttribLocation(this.id, ShaderProgram.ATTRIB_MULTITEXCOORD0, ShaderProgram.ATTRIB_MULTITEXCOORD0_NAME);
}

ShaderProgram.prototype.bindUniforms = function() {
	this.uniformNormal = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_NORMAL_NAME);
	this.uniformColor = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_COLOR_NAME);
	this.uniformModelView = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_MODELVIEW_NAME);
	this.uniformProjection = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_PROJECTION_NAME);
	this.uniformMVP = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_MVP_NAME);
	this.uniformNormalMatrix = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_NORMALMATRIX_NAME);
	this.uniformModelViewT = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_MODELVIEWT_NAME);
	this.uniformLightPosition0 = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_LIGHTPOSITION0_NAME);
	this.uniformTexture0 = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_TEXTURE0_NAME);
	this.uniformTexture1 = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_TEXTURE1_NAME);
	this.uniformTexture2 = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_TEXTURE2_NAME);
	this.uniformTexture3 = gl.getUniformLocation(this.id, ShaderProgram.UNIFORM_TEXTURE3_NAME);
}

ShaderProgram.ATTRIB_VERTEX = 0;
ShaderProgram.ATTRIB_NORMAL = 1;
ShaderProgram.ATTRIB_COLOR = 2;
ShaderProgram.ATTRIB_MULTITEXCOORD0 = 3;
ShaderProgram.ATTRIB_USER = 1000;
ShaderProgram.ATTRIB_VERTEX_NAME = "glt_Vertex";
ShaderProgram.ATTRIB_NORMAL_NAME = "glt_Normal";
ShaderProgram.ATTRIB_COLOR_NAME = "glt_Color";
ShaderProgram.ATTRIB_MULTITEXCOORD0_NAME = "glt_MultiTexCoord0";
ShaderProgram.UNIFORM_NORMAL_NAME = "glt_UniformNormal";
ShaderProgram.UNIFORM_COLOR_NAME = "glt_UniformColor";
ShaderProgram.UNIFORM_MODELVIEW_NAME = "glt_ModelView";
ShaderProgram.UNIFORM_PROJECTION_NAME = "glt_Projection";
ShaderProgram.UNIFORM_MVP_NAME = "glt_MVP";
ShaderProgram.UNIFORM_NORMALMATRIX_NAME = "glt_NormalMatrix";
ShaderProgram.UNIFORM_MODELVIEWT_NAME = "glt_ModelViewT";
ShaderProgram.UNIFORM_LIGHTPOSITION0_NAME = "glt_LightPosition0";
ShaderProgram.UNIFORM_TEXTURE0_NAME = "glt_Texture0";
ShaderProgram.UNIFORM_TEXTURE1_NAME = "glt_Texture1";
ShaderProgram.UNIFORM_TEXTURE2_NAME = "glt_Texture2";
ShaderProgram.UNIFORM_TEXTURE3_NAME = "glt_Texture3";
