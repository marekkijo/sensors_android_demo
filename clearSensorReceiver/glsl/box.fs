precision mediump float;

uniform sampler2D glt_Texture0;

varying vec2 glt_vMultiTexCoord0;

void main() {
	gl_FragColor = texture2D(glt_Texture0, glt_vMultiTexCoord0);
}
