#ifndef WEBSOCKETHANDSHAKER_H
#define WEBSOCKETHANDSHAKER_H

#include <vector>
#include <string>

#define WEBSOCKET_MAGIC_KEY_STRING "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define WEBSOCKET_ENDLINE_TOKEN "\r\n"
#define WEBSOCKET_KEY_TOKEN "Sec-WebSocket-Key: "
#define WEBSOCKET_PROTOCOL_TOKEN "Sec-WebSocket-Protocol: "
#define WEBSOCKET_SEPARATOR_TOKEN ", "

namespace websocket {
    class WebSocketHandshaker
    {
    public:
        WebSocketHandshaker() { }
        bool parseHandshakeRequest(const std::string &req);
        std::string &getHandshakeAccept() { return _accept; }

    private:
        bool extractKey(const std::string &req);
        void extractProtocols(const std::string &req);
        void prepareAcceptKey();
        void prepareHandshake();
        std::string _key;
        std::string _keyAccept;
        std::vector<std::string> _protocols;
        std::string _accept;
    };
} // websocket namespace

#endif // WEBSOCKETHANDSHAKER_H
