#include "websocketserver.h"
#include <sys/socket.h>
#include <cstring>
#include <unistd.h>
#include <errno.h>
#include "websockethandshaker.h"
#include "websocketframeprocessor.h"

using namespace std;
using namespace websocket;

WebSocketServer::WebSocketServer() :
_initialized(false),
_connected(false),
_serverSocket(-1),
_clientSocket(-1) {
    memset(&_serverAddr, 0, sizeof(struct sockaddr_in));
    memset(&_clientAddr, 0, sizeof(struct sockaddr_in));
}

WebSocketServer::~WebSocketServer() {
	destroy();
}

bool WebSocketServer::init(int portno) {
	if (_initialized)
        return true;

    _serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (_serverSocket < 0)
		return false;

    _serverAddr.sin_family = AF_INET;
    _serverAddr.sin_addr.s_addr = INADDR_ANY;
    _serverAddr.sin_port = htons(portno);
    if (bind(_serverSocket, (struct sockaddr *) &_serverAddr, sizeof(struct sockaddr_in)) < 0) {
        shutdown(_serverSocket, SHUT_RDWR);
        close(_serverSocket);
        _serverSocket = -1;
        memset(&_serverAddr, 0, sizeof(struct sockaddr_in));
		return false;
	}

	return _initialized = true;
}

bool WebSocketServer::connect() {
	if (_connected)
		return true;

    socklen_t clientAddrLength = sizeof(struct sockaddr_in);

    listen(_serverSocket, 1);

    _clientSocket = accept(_serverSocket, reinterpret_cast<struct sockaddr *>(&_clientAddr), &clientAddrLength);
    if (_clientSocket < 0)
        return false;

    if (!webSocketHandshake()) {
        shutdown(_clientSocket, SHUT_RDWR);
        close(_clientSocket);
        _clientSocket = -1;
        memset(&_clientAddr, 0, sizeof(struct sockaddr_in));
        return false;
    }

	return _connected = true;
}

void WebSocketServer::destroy() {
	if (!_initialized)
        return;
    if (_connected) {
        shutdown(_clientSocket, SHUT_RDWR);
        close(_clientSocket);
        _clientSocket = -1;
        memset(&_clientAddr, 0, sizeof(struct sockaddr_in));
        _connected = false;
    }
    if (_initialized) {
        shutdown(_serverSocket, SHUT_RDWR);
        close(_serverSocket);
        _serverSocket = -1;
        memset(&_serverAddr, 0, sizeof(struct sockaddr_in));
        _initialized = false;
    }
}

string WebSocketServer::getString() {
    WebSocketFrameProcessor frameProcessor;
    unsigned long long bufferLength = 0;
    unsigned char *buffer = readSocketData(&bufferLength);
    if (!buffer)
        return string();
    frameProcessor.decodeFrame(buffer, bufferLength);
    delete [] buffer;
    return frameProcessor.getString();
}

void WebSocketServer::sendString(const string &str) {
    WebSocketFrameProcessor frameProcessor;
    frameProcessor.encodeString(str);
    writeSocketData(frameProcessor.getFrame(), frameProcessor.getFrameLength());
}

unsigned char *WebSocketServer::readSocketData(unsigned long long *bufferLength, int firstFlags, int flags) {
    unsigned char *buffer = 0;
    ssize_t counter = 0;
    if (bufferLength)
        *bufferLength = 0;
    do {
        unsigned char *tmpBuffer = new unsigned char[SOCKET_DATA_BUFFER_SIZE];
        memset(tmpBuffer, 0, SOCKET_DATA_BUFFER_SIZE);
        counter = recv(_clientSocket, tmpBuffer, SOCKET_DATA_BUFFER_SIZE - 1, firstFlags);
        firstFlags = flags;

        if (counter == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
            delete [] tmpBuffer;
            return buffer;
        }
        if (counter < 0) {
            delete [] buffer;
            delete [] tmpBuffer;
            return 0;
        }

        if (bufferLength)
            *bufferLength += counter;

        if (buffer) {
            unsigned char *concatBuffer = new unsigned char[strlen((char *)buffer) + strlen((char *)tmpBuffer) + 1];
            strcpy((char *)concatBuffer, (char *)buffer);
            strcat((char *)concatBuffer, (char *)tmpBuffer);
            delete [] buffer;
            delete [] tmpBuffer;
            buffer = concatBuffer;
        } else {
            buffer = tmpBuffer;
        }
    } while(counter > 0);
    return buffer;
}

bool WebSocketServer::writeSocketData(const unsigned char *data, unsigned int dataLength) {
    if (!data)
        return false;

    ssize_t counter = 0;
    while (dataLength > 0) {
        counter = send(_clientSocket, data, dataLength, 0);
        if (counter < 0)
            return false;
        data += counter;
        dataLength -= counter;
    }

    return true;
}

bool WebSocketServer::webSocketHandshake() {
    WebSocketHandshaker handshaker;
    unsigned char *buffer = readSocketData();
    if (!buffer)
        return false;

    string share((char *)buffer);
    handshaker.parseHandshakeRequest(share);
    delete [] buffer;

    return writeSocketData((unsigned char *)handshaker.getHandshakeAccept().c_str(), handshaker.getHandshakeAccept().length());
}
