#ifndef WEBSOCKETCRYPTO_HPP
#define WEBSOCKETCRYPTO_HPP

namespace websocket {
    static std::string const base64_chars =
                 "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                 "abcdefghijklmnopqrstuvwxyz"
                 "0123456789+/";

    static inline bool is_base64(unsigned char c) {
        return (c == 43 || // +
               (c >= 47 && c <= 57) || // /-9
               (c >= 65 && c <= 90) || // A-Z
               (c >= 97 && c <= 122)); // a-z
    }

    inline std::string base64_encode(unsigned char const * bytes_to_encode, unsigned int in_len) {
        std::string ret;
        int i = 0;
        int j = 0;
        unsigned char char_array_3[3];
        unsigned char char_array_4[4];

        while (in_len--) {
            char_array_3[i++] = *(bytes_to_encode++);
            if (i == 3) {
                char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
                char_array_4[1] = ((char_array_3[0] & 0x03) << 4) +
                                  ((char_array_3[1] & 0xf0) >> 4);
                char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) +
                                  ((char_array_3[2] & 0xc0) >> 6);
                char_array_4[3] = char_array_3[2] & 0x3f;

                for(i = 0; (i <4) ; i++)
                    ret += base64_chars[char_array_4[i]];
                i = 0;
            }
        }

        if (i) {
            for(j = i; j < 3; j++)
                char_array_3[j] = '\0';

            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) +
                              ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) +
                              ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for (j = 0; (j < i + 1); j++)
                ret += base64_chars[char_array_4[j]];

            while((i++ < 3))
                ret += '=';
        }

        return ret;
    }

    inline std::string base64_encode(std::string const & data) {
        return base64_encode(reinterpret_cast<const unsigned char *>(data.data()),data.size());
    }

    inline std::string base64_decode(std::string const & encoded_string) {
        size_t in_len = encoded_string.size();
        int i = 0;
        int j = 0;
        int in_ = 0;
        unsigned char char_array_4[4], char_array_3[3];
        std::string ret;

        while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
            char_array_4[i++] = encoded_string[in_]; in_++;
            if (i ==4) {
                for (i = 0; i <4; i++)
                    char_array_4[i] = static_cast<unsigned char>(base64_chars.find(char_array_4[i]));

                char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
                char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
                char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

                for (i = 0; (i < 3); i++)
                    ret += char_array_3[i];
                i = 0;
            }
        }

        if (i) {
            for (j = i; j <4; j++)
                char_array_4[j] = 0;

            for (j = 0; j <4; j++)
                char_array_4[j] = static_cast<unsigned char>(base64_chars.find(char_array_4[j]));

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

            for (j = 0; (j < i - 1); j++)
                ret += static_cast<std::string::value_type>(char_array_3[j]);
        }

        return ret;
    }

    namespace { // local namespace
        inline unsigned int rotIntLeft(const unsigned int value, const unsigned int steps) {
            return ((value << steps) | (value >> (32 - steps)));
        }

        inline void clearWBuffert(unsigned int* buffert) {
            for (int pos = 16; --pos >= 0;)
                buffert[pos] = 0;
        }

        inline void innerHash(unsigned int* result, unsigned int* w) {
            unsigned int a = result[0];
            unsigned int b = result[1];
            unsigned int c = result[2];
            unsigned int d = result[3];
            unsigned int e = result[4];

            int round = 0;

    #define SHA1MACRO(func,val) { \
        const unsigned int t = rotIntLeft(a, 5) + (func) + e + val + w[round]; \
        e = d; \
        d = c; \
        c = rotIntLeft(b, 30); \
        b = a; \
        a = t; \
    }

            while (round < 16) {
                SHA1MACRO((b & c) | (~b & d), 0x5a827999)
                ++round;
            }
            while (round < 20) {
                w[round] = rotIntLeft((w[round - 3] ^ w[round - 8] ^ w[round - 14] ^ w[round - 16]), 1);
                SHA1MACRO((b & c) | (~b & d), 0x5a827999)
                ++round;
            }
            while (round < 40) {
                w[round] = rotIntLeft((w[round - 3] ^ w[round - 8] ^ w[round - 14] ^ w[round - 16]), 1);
                SHA1MACRO(b ^ c ^ d, 0x6ed9eba1)
                ++round;
            }
            while (round < 60) {
                w[round] = rotIntLeft((w[round - 3] ^ w[round - 8] ^ w[round - 14] ^ w[round - 16]), 1);
                SHA1MACRO((b & c) | (b & d) | (c & d), 0x8f1bbcdc)
                ++round;
            }
            while (round < 80) {
                w[round] = rotIntLeft((w[round - 3] ^ w[round - 8] ^ w[round - 14] ^ w[round - 16]), 1);
                SHA1MACRO(b ^ c ^ d, 0xca62c1d6)
                ++round;
            }

    #undef SHA1MACRO

            result[0] += a;
            result[1] += b;
            result[2] += c;
            result[3] += d;
            result[4] += e;
        }
    } // local namespace

    /**
     @param src points to any kind of data to be hashed.
     @param bytelength the number of bytes to hash from the src pointer.
     @param hash should point to a buffer of at least 20 bytes of size for storing the sha1 result in.
     */
    inline void calcSHA1(const void* src, const int bytelength, unsigned char* hash) {
        // Init the result array.
        unsigned int result[5] = { 0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0 };

        // Cast the void src pointer to be the byte array we can work with.
        const unsigned char* sarray = (const unsigned char*) src;

        // The reusable round buffer
        unsigned int w[80];

        // Loop through all complete 64byte blocks.
        const int endOfFullBlocks = bytelength - 64;
        int endCurrentBlock;
        int currentBlock = 0;

        while (currentBlock <= endOfFullBlocks) {
            endCurrentBlock = currentBlock + 64;

            // Init the round buffer with the 64 byte block data.
            for (int roundPos = 0; currentBlock < endCurrentBlock; currentBlock += 4) {
                // This line will swap endian on big endian and keep endian on little endian.
                w[roundPos++] = (unsigned int) sarray[currentBlock + 3]
                        | (((unsigned int) sarray[currentBlock + 2]) << 8)
                        | (((unsigned int) sarray[currentBlock + 1]) << 16)
                        | (((unsigned int) sarray[currentBlock]) << 24);
            }
            innerHash(result, w);
        }

        // Handle the last and not full 64 byte block if existing.
        endCurrentBlock = bytelength - currentBlock;
        clearWBuffert(w);
        int lastBlockBytes = 0;
        for (;lastBlockBytes < endCurrentBlock; ++lastBlockBytes)
            w[lastBlockBytes >> 2] |= (unsigned int) sarray[lastBlockBytes + currentBlock] << ((3 - (lastBlockBytes & 3)) << 3);
        w[lastBlockBytes >> 2] |= 0x80 << ((3 - (lastBlockBytes & 3)) << 3);
        if (endCurrentBlock >= 56) {
            innerHash(result, w);
            clearWBuffert(w);
        }
        w[15] = bytelength << 3;
        innerHash(result, w);

        // Store hash in result pointer, and make sure we get in in the correct order on both endian models.
        for (int hashByte = 20; --hashByte >= 0;) {
            hash[hashByte] = (result[hashByte >> 2] >> (((3 - hashByte) & 0x3) << 3)) & 0xff;
        }
    }

    typedef unsigned long long uint64_t;
#if __BYTE_ORDER == __BIG_ENDIAN
    inline uint64_t ntohll(uint64_t v) {
        return v;
    }
#else
    inline uint64_t ntohll(uint64_t v)
    {
      v = ((v & 0x00000000000000FFULL) << 56) |
          ((v & 0x000000000000FF00ULL) << 40) |
          ((v & 0x0000000000FF0000ULL) << 24) |
          ((v & 0x00000000FF000000ULL) <<  8) |
          ((v & 0x000000FF00000000ULL) >>  8) |
          ((v & 0x0000FF0000000000ULL) >> 24) |
          ((v & 0x00FF000000000000ULL) >> 40) |
          ((v & 0xFF00000000000000ULL) >> 56);
      return v;
    }
#endif
} // websocket namespace

#endif // WEBSOCKETCRYPTO_HPP
