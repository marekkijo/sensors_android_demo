#ifndef WEBSOCKETFRAMEPROCESSOR_H
#define WEBSOCKETFRAMEPROCESSOR_H

#include <string>

namespace websocket {
    class WebSocketFrameProcessor {
    public:
        WebSocketFrameProcessor();
        ~WebSocketFrameProcessor();
        // reset have to be called for every decodeFrame/encodeString call
        // at construction WebSocketFrameProcessor is reset
        void reset();
        void decodeFrame(const unsigned char *frame, unsigned long long frameLength);
        void encodeString(const std::string &str);
        bool isDataEnd() { return _frameEnd; }
        std::string &getString() { return _string; }
        unsigned char *getData() { return _data; }
        unsigned long long getDataLength() { return _dataLength; }
        unsigned char *getFrame() { return _frame; }
        unsigned long long getFrameLength() { return _frameLength; }

    private:
        void decodeDataLength(unsigned long long *dataLength, unsigned int *exDataLengthBytes);
        void applyMask(unsigned char *data, const unsigned char *maskKey, unsigned long long len);

        bool _frameEnd;
        unsigned char _lastOpCode;
        unsigned char *_frame;
        unsigned long long _frameLength;
        std::string _string;
        unsigned char *_data;
        unsigned long long _dataLength;
    };
} // websocket namespace

#endif // WEBSOCKETFRAMEPROCESSOR_H
