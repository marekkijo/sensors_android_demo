#include "websockethandshaker.h"
#include <cstring>
#include "websocketcrypto.hpp"

using namespace std;
using namespace websocket;

bool WebSocketHandshaker::parseHandshakeRequest(const std::string &req) {
    _key = "";
    _keyAccept = "";
    _protocols.clear();
    _accept = "";

    if (!extractKey(req))
        return false;
    extractProtocols(req);
    prepareAcceptKey();
    prepareHandshake();

    return true;
}

bool WebSocketHandshaker::extractKey(const std::string &req) {
    size_t beginPos, endPos;

    beginPos = req.find(WEBSOCKET_KEY_TOKEN);
    if (beginPos == string::npos)
        return false;
    beginPos += strlen(WEBSOCKET_KEY_TOKEN);

    endPos = req.find(WEBSOCKET_ENDLINE_TOKEN, beginPos);
    if (endPos == string::npos)
        return false;

    _key = req.substr(beginPos, endPos - beginPos);
    return true;
}

void WebSocketHandshaker::extractProtocols(const std::string &req) {
    size_t beginPos, endPos, separatorPos;

    beginPos = req.find(WEBSOCKET_PROTOCOL_TOKEN);
    if (beginPos == string::npos)
        return;
    beginPos += strlen(WEBSOCKET_PROTOCOL_TOKEN);

    endPos = req.find(WEBSOCKET_ENDLINE_TOKEN, beginPos);
    if (endPos == string::npos)
        return;

    for(;;) {
        separatorPos = req.find(WEBSOCKET_SEPARATOR_TOKEN, beginPos);
        if (separatorPos == string::npos || separatorPos > endPos) {
            _protocols.push_back(req.substr(beginPos, endPos - beginPos));
            return;
        } else {
            _protocols.push_back(req.substr(beginPos, separatorPos - beginPos));
            beginPos = separatorPos + strlen(WEBSOCKET_SEPARATOR_TOKEN);
        }
    }
}

void WebSocketHandshaker::prepareAcceptKey() {
    string keyStr = _key + WEBSOCKET_MAGIC_KEY_STRING;
    unsigned char *buffer = new unsigned char[keyStr.length()];
    unsigned char sha1[20];

    memset(sha1, 0, 20);
    memcpy(buffer, keyStr.c_str(), keyStr.length());

    calcSHA1(buffer, keyStr.length(), sha1);
    _keyAccept = base64_encode(sha1, 20);

    delete [] buffer;
}

void WebSocketHandshaker::prepareHandshake() {
    _accept = "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: " + _keyAccept + "\r\n";
    if (_protocols.size() > 0)
        _accept += "Sec-WebSocket-Protocol: " + _protocols[0] + "\r\n";
    _accept += "\r\n";
}
