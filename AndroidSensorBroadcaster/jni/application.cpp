#include "application.h"
#include <new>
#include <android_native_app_glue.h>
#include <android/sensor.h>
#include <android/log.h>
#include "egldisplaymanager.h"
#include <GLES/gl.h>

#define LOGV(...) ((void)__android_log_print(ANDROID_LOG_VERBOSE, "SensorBroadcaster", __VA_ARGS__))
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "SensorBroadcaster", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "SensorBroadcaster", __VA_ARGS__))

using namespace websocket;

Application::Application(struct android_app *androidApp) :
_androidApp(androidApp),
_initialized(false),
_displayManager(nullptr),
_wss(nullptr),
_sensorManager(nullptr),
_accelerometerSensor(nullptr),
_gyroscopeSensor(nullptr),
_sensorEventQueue(nullptr) {
}

Application::~Application() {
	delete _displayManager;
	delete _wss;
}

bool Application::init() {
	if (_initialized)
		return true;
	app_dummy();

	_androidApp->userData = this;
	_androidApp->onAppCmd = Application::onAppCmdCallback;
	_androidApp->onInputEvent = Application::onInputEventCallback;
	
	_displayManager = new (std::nothrow) EGLDisplayManager();
	_wss = new (std::nothrow) WebSocketServer();

	_sensorManager = ASensorManager_getInstance();
	_accelerometerSensor = ASensorManager_getDefaultSensor(_sensorManager, ASENSOR_TYPE_ACCELEROMETER);
	_gyroscopeSensor = ASensorManager_getDefaultSensor(_sensorManager, ASENSOR_TYPE_GYROSCOPE);
	_sensorEventQueue = ASensorManager_createEventQueue(_sensorManager, _androidApp->looper, LOOPER_ID_USER, NULL, this);

	return _initialized = true;
}

void Application::start() {
	if (!_initialized)
		return;
	while (1) {
		int ident, events;
		struct android_poll_source *source;

		while ((ident = ALooper_pollAll(0, 0, &events, (void **)&source)) >= 0) {
			if (source)
				source->process(_androidApp, source);
			if (_androidApp->destroyRequested) {
				_displayManager->destroy();
				return;
			}
		}
	}
}

void Application::onAppCmdCallback(struct android_app *androidApp, int cmd) {
	static_cast<Application *>(androidApp->userData)->onAppCmd(cmd);
}

void Application::onAppCmd(int cmd) {
	switch (cmd) {
		// the AInputQueue has changed. Upon processing this command, android_app->inputQueue will be updated to the new queue (or NULL).
		case APP_CMD_INPUT_CHANGED:
		break;
		// a new ANativeWindow is ready for use. Upon receiving this command, android_app->window will contain the new window surface.
		case APP_CMD_INIT_WINDOW:
		if (_displayManager->init(_androidApp->window)) {
			glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
			glEnable(GL_CULL_FACE);
			//glShadeModel(GL_SMOOTH);
			glDisable(GL_DEPTH_TEST);
			glClearColor(0.5, 0.5, 0.5, 1.0);
			glClear(GL_COLOR_BUFFER_BIT);
			_displayManager->swapBuffers();
		}
		_wss->init(1234);
		break;
		// the existing ANativeWindow needs to be terminated. Upon receiving this command, android_app->window still contains the existing window; after calling android_app_exec_cmd it will be set to NULL.
		case APP_CMD_TERM_WINDOW:
		_displayManager->destroy();
		break;
		// the current ANativeWindow has been resized. Please redraw with its new size.
		case APP_CMD_WINDOW_RESIZED:
		break;
		// the system needs that the current ANativeWindow be redrawn. You should redraw the window before handing this to android_app_exec_cmd() in order to avoid transient drawing glitches.
		case APP_CMD_WINDOW_REDRAW_NEEDED:
		break;
		// the content area of the window has changed, such as from the soft input window being shown or hidden. You can find the new content rect in android_app::contentRect.
		case APP_CMD_CONTENT_RECT_CHANGED:
		break;
		// the app's activity window has gained input focus.
		case APP_CMD_GAINED_FOCUS:
		if (_displayManager->isInitialized()) {
			glClearColor(1.0, 0.5, 0.5, 1.0);
			glClear(GL_COLOR_BUFFER_BIT);
			_displayManager->swapBuffers();
		}
		if (_wss->isInitialized()) {
			if (_wss->connect() && _displayManager->isInitialized()) {
				if (_accelerometerSensor) {
					ASensorEventQueue_enableSensor(_sensorEventQueue, _accelerometerSensor);
					ASensorEventQueue_setEventRate(_sensorEventQueue, _accelerometerSensor, (1000L/60)*1000);
				}
				if (_gyroscopeSensor) {
					ASensorEventQueue_enableSensor(_sensorEventQueue, _gyroscopeSensor);
					ASensorEventQueue_setEventRate(_sensorEventQueue, _gyroscopeSensor, (1000L/60)*1000);
				}
				glClearColor(0.5, 1.0, 0.5, 1.0);
				glClear(GL_COLOR_BUFFER_BIT);
				_displayManager->swapBuffers();
			}
		}
		break;
		// the app's activity window has lost input focus.
		case APP_CMD_LOST_FOCUS:
		if (_accelerometerSensor) {
			ASensorEventQueue_disableSensor(_sensorEventQueue, _accelerometerSensor);
		}
		glClear(GL_COLOR_BUFFER_BIT);
		_displayManager->swapBuffers();
		break;
		// the current device configuration has changed.
		case APP_CMD_CONFIG_CHANGED:
		break;
		// the system is running low on memory. Try to reduce your memory use.
		case APP_CMD_LOW_MEMORY:
		break;
		// the app's activity has been started.
		case APP_CMD_START:
		break;
		// the app's activity has been resumed.
		case APP_CMD_RESUME:
		break;
		// the app should generate a new saved state for itself, to restore from later if needed. If you have saved state, allocate it with malloc and place it in android_app.savedState with the size in android_app.savedStateSize. The will be freed for you later.
		case APP_CMD_SAVE_STATE:
		break;
		// the app's activity has been paused.
		case APP_CMD_PAUSE:
		break;
		// the app's activity has been stopped.
		case APP_CMD_STOP:
		break;
		// the app's activity is being destroyed, and waiting for the app thread to clean up and exit before proceeding.
		case APP_CMD_DESTROY:
		break;
		default:
		break;
	}
}

int Application::onInputEventCallback(struct android_app *androidApp, AInputEvent *event) {
	return static_cast<Application *>(androidApp->userData)->onInputEvent(event);
}

int Application::onInputEvent(AInputEvent *event) {
	switch (AInputEvent_getType(event)) {
		// indicates that the input event is a key event.
		case AINPUT_EVENT_TYPE_KEY:
		break;
		// indicates that the input event is a motion event.
		case AINPUT_EVENT_TYPE_MOTION:
		break;
		default:
		break;
	}
}

int Application::onSensorEventCallback(int fd, int events, void *data) {
	return static_cast<Application *>(data)->onSensorEvent(fd, events);
}

int Application::onSensorEvent(int fd, int events) {
	return 0;
}
