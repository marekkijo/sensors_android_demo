#ifndef EGLDISPLAYMANAGER_H
#define EGLDISPLAYMANAGER_H

#include <EGL/egl.h>

class EGLDisplayManager {
public:
	EGLDisplayManager();
	virtual ~EGLDisplayManager();
	bool isInitialized() { return _initialized; };
	bool init(EGLNativeWindowType native_window);
	void destroy();
	void swapBuffers();

private:
	void resetEGL();

	bool _initialized;
	EGLDisplay _display;
	EGLSurface _surface;
	EGLContext _context;
	EGLint _width;
	EGLint _height;
};

#endif
