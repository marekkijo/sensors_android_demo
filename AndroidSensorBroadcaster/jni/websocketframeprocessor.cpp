#include "websocketframeprocessor.h"
#include <cstring>
#include <netinet/in.h>
#include "websocketcrypto.hpp"

using namespace std;
using namespace websocket;

WebSocketFrameProcessor::WebSocketFrameProcessor() :
    _frameEnd(true),
    _lastOpCode(0xFF),
    _frame(0),
    _frameLength(0),
    _string(),
    _data(0),
    _dataLength(0) {
}

WebSocketFrameProcessor::~WebSocketFrameProcessor() {
    reset();
}

void WebSocketFrameProcessor::reset() {
    _frameEnd = true;
    _lastOpCode = 0xFF;
    delete [] _frame;
    _frame = 0;
    _frameLength = 0;
    _string = string();
    delete [] _data;
    _data = 0;
    _dataLength = 0;
}

void WebSocketFrameProcessor::decodeFrame(const unsigned char *frame, unsigned long long frameLength) {
    if (!frame || frameLength <= 0) return;

    _frameLength = frameLength;
    _frame = new unsigned char[_frameLength];
    memcpy(_frame, frame, frameLength);

    // fin bit - if current frame is final/complete
    _frameEnd = _frame[0] & 0x80;
    // mask bit - if data is masked (4 bytes mask - XOR with data)
    bool masked = _frame[1] & 0x80;
    // opcode - operation code
    unsigned char opcode = _frame[0] & 0xF;

    // length of data
    unsigned long long dataLength = 0;
    unsigned int exDataLengthBytes = 0;
    decodeDataLength(&dataLength, &exDataLengthBytes);

    // clear data - copy of data (possibly demasked if mask is used)
    unsigned char *clearData = 0;
    if (dataLength > 0) {
        const unsigned char *dataPosition = _frame + 2 + exDataLengthBytes + (masked ? 4 : 0);
        clearData = new unsigned char[dataLength];
        memcpy(clearData, dataPosition, dataLength);
        if (masked) {
            const unsigned char *maskKeyPosition = _frame + 2 + exDataLengthBytes;
            applyMask(clearData, maskKeyPosition, dataLength);
        }
    }

    switch (opcode) {
    case 0x0: // continuation frame
        switch (_lastOpCode) {
        case 0x1: // text frame
            if (clearData)
                _string.append((char *)clearData, dataLength);
            break;
        case 0x2: // binary frame
            if (clearData) {
                unsigned char *tmpData = new unsigned char[_dataLength + dataLength];
                memcpy(tmpData, _data, _dataLength);
                delete [] _data;
                memcpy(tmpData + _dataLength, clearData, dataLength);
                _data = tmpData;
                _dataLength += dataLength;
            }
        default:
            break;
        }
        break;
    case 0x1: // text frame
        if (clearData)
            _string = string((char *)clearData, dataLength);
        break;
    case 0x2: // binary frame
        if (clearData) {
            _data = new unsigned char[dataLength];
            memcpy(_data, clearData, dataLength);
            _dataLength = dataLength;
        }
        break;
    case 0x8: // connection close
    case 0x9: // ping
    case 0xA: // pong
    default:
        break;
    }

    delete [] clearData;

    if (opcode != 0x0) // not a continuation
        _lastOpCode = opcode;
}

void WebSocketFrameProcessor::encodeString(const string &str) {
    if (str.empty())
        return;

    _string = str;

    int dataOffset = 0;
    if (_string.length() < 125)
        dataOffset = 2;
    else if (_string.length() > 125 && _string.length() < 65535)
        dataOffset = 4;
    else
        dataOffset = 10;

    _frameLength = dataOffset + _string.length();

    _frame = new unsigned char[_frameLength];
    memset(_frame, 0, dataOffset);
    memcpy(_frame + dataOffset, str.c_str(), str.length());

    _frame[0] = 0x80 + 0x1; // fin + opcode=text

    if (dataOffset == 2) {
        _frame[1] = str.length();
    } else if (dataOffset == 4) {
        _frame[1] = 126;
        uint16_t len = _string.length();
        *((uint16_t *)(_frame + 2)) = htons(len);
    } else {
        _frame[1] = 127;
        uint64_t len = _string.length();
        *((uint64_t *)(_frame + 2)) = ntohll(len);
    }
}

void WebSocketFrameProcessor::decodeDataLength(unsigned long long *dataLength, unsigned int *exDataLengthBytes) {
    *dataLength = _frame[1] & 0x7F;
    if (*dataLength == 126) {
        *exDataLengthBytes = 2;
        *dataLength = ntohs(*((uint16_t *)(_frame + 2)));
    } else if (*dataLength == 127) {
        *exDataLengthBytes = 8;
        *dataLength = ntohll(*((uint64_t *)(_frame + 2)));
    } else {
        *exDataLengthBytes = 0;
    }
}

void WebSocketFrameProcessor::applyMask(unsigned char *data, const unsigned char *maskKey, unsigned long long len) {
    for (unsigned long long i = 0; i < len; ++ i)
        data[i] ^= *(maskKey + (i%4));
}
