#include "application.h"

struct android_app;

extern "C" {
   void android_main(struct android_app *state);
}

void android_main(struct android_app *state) {
	Application app(state);
	if (app.init())
		app.start();
}
