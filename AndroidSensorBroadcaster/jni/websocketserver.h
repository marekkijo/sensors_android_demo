#ifndef WEBSOCKETSERVER_H
#define WEBSOCKETSERVER_H

#include <string>
#include <netinet/in.h>

#define SOCKET_DATA_BUFFER_SIZE 256

namespace websocket {
class WebSocketServer {
public:
    WebSocketServer();
    virtual ~WebSocketServer();
    bool isInitialized() { return _initialized; }
    bool isConnected() { return _connected; }
	bool init(int portno = 1234);
	bool connect();
    void destroy();
    std::string getString();
    void sendString(const std::string &str);

private:
    unsigned char *readSocketData(unsigned long long *bufferLength = 0, int firstFlags = 0, int flags = MSG_DONTWAIT);
    bool writeSocketData(const unsigned char *data, unsigned int dataLength);
    bool webSocketHandshake();

	bool _initialized;
	bool _connected;
    int _serverSocket;
    struct sockaddr_in _serverAddr;
    int _clientSocket;
    struct sockaddr_in _clientAddr;
};
} // websocket namespace

#endif // WEBSOCKETSERVER_H
