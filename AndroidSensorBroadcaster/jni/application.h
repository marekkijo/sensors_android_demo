#ifndef APPLICATION_H
#define APPLICATION_H

#include "websocketserver.h"

struct android_app;
class AInputEvent;
class EGLDisplayManager;
class ASensorManager;
class ASensor;
class ASensorEventQueue;

class Application {
public:
	Application(struct android_app *androidApp);
	virtual ~Application();
	bool init();
	void start();

private:
	static void onAppCmdCallback(struct android_app *androidApp, int cmd);
	void onAppCmd(int cmd);
	static int onInputEventCallback(struct android_app *androidApp, AInputEvent *event);
	int onInputEvent(AInputEvent *event);
	static int onSensorEventCallback(int fd, int events, void *data);
	int onSensorEvent(int fd, int events);

	struct android_app *_androidApp;
	bool _initialized;

	EGLDisplayManager *_displayManager;
	websocket::WebSocketServer *_wss;
	ASensorManager *_sensorManager;
	const ASensor *_accelerometerSensor;
	const ASensor *_gyroscopeSensor;
    ASensorEventQueue *_sensorEventQueue;
};

#endif
