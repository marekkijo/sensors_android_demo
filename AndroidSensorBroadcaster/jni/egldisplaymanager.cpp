#include "egldisplaymanager.h"

EGLDisplayManager::EGLDisplayManager() :
_initialized(false),
_display(EGL_NO_DISPLAY),
_surface(EGL_NO_SURFACE),
_context(EGL_NO_CONTEXT),
_width(0),
_height(0) {
}

EGLDisplayManager::~EGLDisplayManager() {
	destroy();
}

bool EGLDisplayManager::init(EGLNativeWindowType native_window) {
	if (!native_window)
		return false;
	if (_initialized)
		return true;
	const EGLint attrib_list[] = {
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 8,
		EGL_NONE
	};
	EGLConfig configs;
	EGLint num_config;

	_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	eglInitialize(_display, 0, 0);
	eglChooseConfig(_display, attrib_list, &configs, 1, &num_config);
	_surface = eglCreateWindowSurface(_display, configs, native_window, 0);
	_context = eglCreateContext(_display, configs, 0, 0);

	if (!eglMakeCurrent(_display, _surface, _surface, _context)) {
		resetEGL();
		return false;
	}

	eglQuerySurface(_display, _surface, EGL_WIDTH, &_width);
	eglQuerySurface(_display, _surface, EGL_HEIGHT, &_height);

	return _initialized = true;
}

void EGLDisplayManager::destroy() {
	if (!_initialized)
		return;
	resetEGL();
	_initialized = false;
}

void EGLDisplayManager::swapBuffers() {
	if (_initialized)
		eglSwapBuffers(_display, _surface);
}

void EGLDisplayManager::resetEGL() {
	if (_display != EGL_NO_DISPLAY) {
		eglMakeCurrent(_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		if (_context != EGL_NO_CONTEXT)
			eglDestroyContext(_display, _context);
		if (_surface != EGL_NO_SURFACE)
			eglDestroySurface(_display, _surface);
		eglTerminate(_display);
	}
	_display = EGL_NO_DISPLAY;
	_surface = EGL_NO_SURFACE;
	_context = EGL_NO_CONTEXT;
	_width = _height = 0;
}
