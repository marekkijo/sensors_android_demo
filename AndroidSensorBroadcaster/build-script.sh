#!/bin/bash
rm -rf bin/ build.xml libs/ local.properties obj/ proguard-project.txt project.properties
echo -e "target=android-19\n" >> default.properties
android update project -p .
ndk-build
ant debug
adb install bin/NativeActivity-debug.apk
adb shell am start -n com.marekkijo.sensor_broadcaster/android.app.NativeActivity
