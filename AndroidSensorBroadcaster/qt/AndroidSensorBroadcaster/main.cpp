#include "websocketserver.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <unistd.h>

using namespace std;

int main()
{
    srand(time(0));
    ostringstream strs;
    websocket::WebSocketServer wss;
    wss.init(1234);
    wss.connect();
    cout << wss.getString() << endl;
    for (int i = 0; i < 100; ++ i) {
        strs.str("");
        strs.clear();
        strs << "a " <<
                ((double) rand() / (RAND_MAX)) << " " <<
                ((double) rand() / (RAND_MAX)) << " " <<
                ((double) rand() / (RAND_MAX));
        wss.sendString(strs.str());
        cout << strs.str() << endl;
    }
    wss.sendString(string("DATAdata"));
    sleep(15);
    return 0;
}
