TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cpp \
    ../../jni/websocketserver.cpp \
    ../../jni/websockethandshaker.cpp \
    ../../jni/websocketframeprocessor.cpp

HEADERS += \
    ../../jni/websocketserver.h \
    ../../jni/websockethandshaker.h \
    ../../jni/websocketcrypto.hpp \
    ../../jni/websocketframeprocessor.h

INCLUDEPATH += \
    ../../jni/
